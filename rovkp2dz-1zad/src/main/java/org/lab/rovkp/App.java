package org.lab.rovkp;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class App {
    public static final String CSV_SEPARATOR = ",";
    private static final int PROGRESS_REFRESH_RATE = 333;

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Please specify location of input data and output respectively");
            System.exit(1);
        }

        Job job = Job.getInstance();
        job.setJarByClass(App.class);
        job.setJobName("Taxi ride duration - info");

        Path inputPath = new Path(args[0]);
        Path outputPath = new Path(args[1]);
        FileInputFormat.addInputPath(job, inputPath);
        FileOutputFormat.setOutputPath(job, outputPath);

        job.setInputFormatClass(TextInputFormat.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(TaxiRideDurationInfo.class);

        job.setMapperClass(TaxiRideDurationMapper.class);
        job.setCombinerClass(TaxiRideDurationReducer.class);
        job.setReducerClass(TaxiRideDurationReducer.class);

        long startTime = System.currentTimeMillis();
        job.submit();
        boolean isSuccess = waitAndPrintJobProgress(job);
        System.out.println(String.format("Execution took %dms", System.currentTimeMillis() - startTime));
        System.out.println(String.format("MapReduce job was %s", isSuccess ? "successful" : "NOT successful"));
    }

    private static boolean waitAndPrintJobProgress(Job job) throws IOException, InterruptedException {
        while (true) {
            System.out.print(String.format("\rMap progress %.2f%%\tReduce progress %.2f%%", job.mapProgress() * 100f, job.reduceProgress() * 100f));
            if (job.isComplete()) {
                System.out.print("\n");
                return job.isSuccessful();
            }

            Thread.sleep(App.PROGRESS_REFRESH_RATE);
        }
    }
}
