package org.lab.rovkp;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TaxiRideDurationInfo implements WritableComparable<TaxiRideDurationInfo> {

    private IntWritable totalTripDuration;
    private IntWritable minTripDuration;
    private IntWritable maxTripDuration;

    public TaxiRideDurationInfo() {
        this.totalTripDuration = new IntWritable();
        this.minTripDuration = new IntWritable();
        this.maxTripDuration = new IntWritable();
    }

    public int getTotalTripDuration() {
        return totalTripDuration.get();
    }

    public void setTotalTripDuration(int totalTripDuration) {
        this.totalTripDuration.set(totalTripDuration);
    }

    public int getMinTripDuration() {
        return minTripDuration.get();
    }

    public void setMinTripDuration(int minTripDuration) {
        this.minTripDuration.set(minTripDuration);
    }

    public int getMaxTripDuration() {
        return maxTripDuration.get();
    }

    public void setMaxTripDuration(int maxTripDuration) {
        this.maxTripDuration.set(maxTripDuration);
    }

    @Override
    public int compareTo(TaxiRideDurationInfo o) {
        return 0;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        totalTripDuration.write(dataOutput);
        minTripDuration.write(dataOutput);
        maxTripDuration.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        totalTripDuration.readFields(dataInput);
        minTripDuration.readFields(dataInput);
        maxTripDuration.readFields(dataInput);
    }

    @Override
    public String toString() {
        return String.format("total:%d min:%d max:%d",
                totalTripDuration.get(), minTripDuration.get(), maxTripDuration.get());
    }
}
