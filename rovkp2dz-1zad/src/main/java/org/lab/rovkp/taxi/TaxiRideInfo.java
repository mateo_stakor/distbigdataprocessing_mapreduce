package org.lab.rovkp.taxi;

import java.util.Date;

public class TaxiRideInfo {

    private String medallion;
    private int duration;

    public String getMedallion() {
        return medallion;
    }

    public void setMedallion(String medallion) {
        this.medallion = medallion;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
