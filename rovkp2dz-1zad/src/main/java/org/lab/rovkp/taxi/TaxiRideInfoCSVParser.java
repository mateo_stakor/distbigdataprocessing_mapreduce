package org.lab.rovkp.taxi;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TaxiRideInfoCSVParser {

    public static TaxiRideInfo parse(String line, String separator) {
        return parse(line.split(separator));
    }

    public static TaxiRideInfo parse(String[] columns) {
        if(columns == null || columns.length < 10) return null;

        TaxiRideInfo taxiRideInfo = new TaxiRideInfo();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        taxiRideInfo.setMedallion(columns[0]);
        try {
            taxiRideInfo.setDuration(Integer.parseInt(columns[8]));
        } catch (NumberFormatException e) {
            taxiRideInfo.setDuration(0);
        }

        return taxiRideInfo;
    }
}
