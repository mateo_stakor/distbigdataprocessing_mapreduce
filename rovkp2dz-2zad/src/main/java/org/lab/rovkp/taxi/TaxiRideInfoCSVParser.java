package org.lab.rovkp.taxi;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TaxiRideInfoCSVParser {

    public static TaxiRideInfo parse(String line, String separator) {
        return parse(line.split(separator));
    }

    public static TaxiRideInfo parse(String[] columns) {
        if(columns == null || columns.length < 10) return null;

        TaxiRideInfo taxiRideInfo = new TaxiRideInfo();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        taxiRideInfo.setMedallion(columns[0]);
        taxiRideInfo.setHackLicense(columns[1]);
        taxiRideInfo.setVendorId(columns[2]);
        taxiRideInfo.setRateCode(columns[3]);
        taxiRideInfo.setStoreAndFwdFlag(columns[4]);
        try {
            taxiRideInfo.setPickupDateTime(dateFormatter.parse(columns[5]));
            taxiRideInfo.setDropOffDateTime(dateFormatter.parse(columns[6]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        taxiRideInfo.setPassengerCount(Integer.parseInt(columns[7]));
        taxiRideInfo.setTripTimeInSeconds(Integer.parseInt(columns[8]));
        taxiRideInfo.setTripDistance(Double.parseDouble(columns[9]));
        taxiRideInfo.setPickupLatitude(Double.parseDouble(columns[11]));
        taxiRideInfo.setPickupLongitude(Double.parseDouble(columns[10]));
        taxiRideInfo.setDropOffLatitude(Double.parseDouble(columns[13]));
        taxiRideInfo.setDropOffLatitude(Double.parseDouble(columns[12]));

        return taxiRideInfo;
    }
}
