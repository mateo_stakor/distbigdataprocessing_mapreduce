package org.lab.rovkp;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class App {
    public static final String CSV_SEPARATOR = ",";
    private static final int PROGRESS_REFRESH_RATE = 333;
    private static final String INTERMEDIATE_PATH = "intermediate";

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Please specify location of input data and output respectively");
            System.exit(1);
        }

        Job splitByLocationAndPassengerCountJob = Job.getInstance();
        splitByLocationAndPassengerCountJob.setJarByClass(App.class);
        splitByLocationAndPassengerCountJob.setJobName("Taxi ride spltting");

        Path inputPath = new Path(args[0]);
        Path outputPath = new Path(INTERMEDIATE_PATH);
        FileInputFormat.addInputPath(splitByLocationAndPassengerCountJob, inputPath);
        FileOutputFormat.setOutputPath(splitByLocationAndPassengerCountJob, outputPath);

        splitByLocationAndPassengerCountJob.setInputFormatClass(TextInputFormat.class);

        splitByLocationAndPassengerCountJob.setMapperClass(TaxiRideTripMapper.class);
        splitByLocationAndPassengerCountJob.setPartitionerClass(TaxiRideTripPartitioner.class);
        splitByLocationAndPassengerCountJob.setReducerClass(TaxiRideTripReducer.class);
        splitByLocationAndPassengerCountJob.setNumReduceTasks(6);

        long startTime = System.currentTimeMillis();
        splitByLocationAndPassengerCountJob.submit();
        boolean isSuccess = waitAndPrintJobProgress(splitByLocationAndPassengerCountJob);
        System.out.println(String.format("Execution took %dms", System.currentTimeMillis() - startTime));
        System.out.println(String.format("MapReduce job was %s", isSuccess ? "successful" : "NOT successful"));

        if (isSuccess) {
            Job minMaxTaxiDurationJob = Job.getInstance();
            minMaxTaxiDurationJob.setJarByClass(App.class);
            minMaxTaxiDurationJob.setJobName("Taxi ride duration - info");

            Path minMaxTaxiDurationJobInputPath = new Path(INTERMEDIATE_PATH);
            Path minMaxTaxiDurationJobOutputPath = new Path(args[1]);
            FileInputFormat.addInputPath(minMaxTaxiDurationJob, minMaxTaxiDurationJobInputPath);
            FileOutputFormat.setOutputPath(minMaxTaxiDurationJob, minMaxTaxiDurationJobOutputPath);

            minMaxTaxiDurationJob.setInputFormatClass(TextInputFormat.class);

            minMaxTaxiDurationJob.setMapOutputKeyClass(Text.class);
            minMaxTaxiDurationJob.setMapOutputValueClass(TaxiRideDurationInfo.class);

            minMaxTaxiDurationJob.setMapperClass(TaxiRideDurationMapper.class);
            minMaxTaxiDurationJob.setCombinerClass(TaxiRideDurationReducer.class);
            minMaxTaxiDurationJob.setReducerClass(TaxiRideDurationReducer.class);

            long startTime1 = System.currentTimeMillis();
            minMaxTaxiDurationJob.submit();
            waitAndPrintJobProgress(minMaxTaxiDurationJob);
            System.out.println(String.format("Execution took %dms", System.currentTimeMillis() - startTime1));
            System.out.println(String.format("MapReduce job was %s", isSuccess ? "successful" : "NOT successful"));
        }

        FileSystem.get(splitByLocationAndPassengerCountJob.getConfiguration())
                .delete(new Path(INTERMEDIATE_PATH), true);
    }

    private static boolean waitAndPrintJobProgress(Job job) throws IOException, InterruptedException {
        while (true) {
            System.out.print(String.format("\rMap progress %.2f%%\tReduce progress %.2f%%", job.mapProgress() * 100f, job.reduceProgress() * 100f));
            if (job.isComplete()) {
                System.out.print("\n");
                return job.isSuccessful();
            }

            Thread.sleep(App.PROGRESS_REFRESH_RATE);
        }
    }
}
