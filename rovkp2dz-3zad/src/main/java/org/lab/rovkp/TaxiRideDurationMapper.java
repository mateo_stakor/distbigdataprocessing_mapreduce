package org.lab.rovkp;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.lab.rovkp.taxi.TaxiRideInfo;
import org.lab.rovkp.taxi.TaxiRideInfoCSVParser;

import java.io.IOException;

public class TaxiRideDurationMapper extends Mapper<LongWritable, Text, Text, TaxiRideDurationInfo> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        TaxiRideInfo taxiRideInfo = TaxiRideInfoCSVParser.parse(value.toString(), App.CSV_SEPARATOR);

        if (key.get() > 0) {
            if (taxiRideInfo != null) {
                TaxiRideDurationInfo taxiRideDurationInfo = new TaxiRideDurationInfo();
                taxiRideDurationInfo.setTotalTripDuration(taxiRideInfo.getTripTimeInSeconds());
                taxiRideDurationInfo.setMinTripDuration(taxiRideInfo.getTripTimeInSeconds());
                taxiRideDurationInfo.setMaxTripDuration(taxiRideInfo.getTripTimeInSeconds());

                context.write(new Text(taxiRideInfo.getMedallion()), taxiRideDurationInfo);
            }
        }
    }
}
