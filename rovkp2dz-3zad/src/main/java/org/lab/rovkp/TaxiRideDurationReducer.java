package org.lab.rovkp;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class TaxiRideDurationReducer extends Reducer<Text, TaxiRideDurationInfo, Text, TaxiRideDurationInfo> {

    @Override
    protected void reduce(Text key, Iterable<TaxiRideDurationInfo> values, Context context) throws IOException, InterruptedException {

        TaxiRideDurationInfo taxiRideDurationInfo = new TaxiRideDurationInfo();
        taxiRideDurationInfo.setMinTripDuration(Integer.MAX_VALUE);
        taxiRideDurationInfo.setMaxTripDuration(0);
        taxiRideDurationInfo.setTotalTripDuration(0);

        for(TaxiRideDurationInfo val : values) {
            taxiRideDurationInfo.setTotalTripDuration(
                    taxiRideDurationInfo.getTotalTripDuration() + val.getTotalTripDuration()
            );

            if (taxiRideDurationInfo.getMinTripDuration() > val.getMinTripDuration())
                taxiRideDurationInfo.setMinTripDuration(val.getMinTripDuration());
            if (taxiRideDurationInfo.getMaxTripDuration() < val.getMaxTripDuration())
                taxiRideDurationInfo.setMaxTripDuration(val.getMaxTripDuration());
        }

        context.write(key, taxiRideDurationInfo);
    }
}
