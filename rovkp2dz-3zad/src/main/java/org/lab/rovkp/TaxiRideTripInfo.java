package org.lab.rovkp;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TaxiRideTripInfo implements WritableComparable<TaxiRideTripInfo> {

    private Text line;
    private IntWritable passangerCount;
    private DoubleWritable dropOffLat;
    private DoubleWritable dropOffLng;
    private DoubleWritable pickupLat;
    private DoubleWritable pickupLng;


    @Override
    public int compareTo(TaxiRideTripInfo o) {
        return 0;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        line.write(dataOutput);
        passangerCount.write(dataOutput);
        dropOffLat.write(dataOutput);
        dropOffLng.write(dataOutput);
        pickupLat.write(dataOutput);
        pickupLng.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {

    }
}
