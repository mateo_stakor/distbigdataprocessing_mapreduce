package org.lab.rovkp;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;
import org.lab.rovkp.taxi.TaxiRideInfo;
import org.lab.rovkp.taxi.TaxiRideInfoCSVParser;

public class TaxiRideTripPartitioner extends Partitioner<LongWritable, Text> {
    @Override
    public int getPartition(LongWritable key, Text value, int i) {
        TaxiRideInfo taxiRideInfo = TaxiRideInfoCSVParser.parse(value.toString(), App.CSV_SEPARATOR);
        return hash(taxiRideInfo);
    }

    private boolean isInsideBoundaries(double lat, double lng) {
        return lng >= -74 && lng <= -73.95 && lat <= 40.8 && lat >= 40.75;
    }

    private int hashNumOfPassengers(TaxiRideInfo taxiRideInfo) {
        if (taxiRideInfo.getPassengerCount() == 1) return 1;
        else if (taxiRideInfo.getPassengerCount() == 2 || taxiRideInfo.getPassengerCount() == 3)
            return 2;
        else return 3;
    }

    private int hash(TaxiRideInfo taxiRideInfo) {
        int i = isInsideBoundaries(taxiRideInfo.getPickupLatitude(), taxiRideInfo.getPickupLongitude()) &&
            isInsideBoundaries(taxiRideInfo.getDropOffLatitude(), taxiRideInfo.getDropOffLongitude()) ? -1 : 2;
        return i + hashNumOfPassengers(taxiRideInfo);
    }
}
